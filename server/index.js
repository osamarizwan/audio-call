const path = require('path');
const cors = require('cors');
const express = require('express');
const ws = require('ws');

const app = express();

const wsserver = ws.Server;
const wss = new wsserver({ port: process.env.PORT });

var users = {};
var otherUser;
wss.on('connection', function (conn) {
  console.log('User connected');
  conn.on('message', function (message) {
    var data = JSON.parse(message);
    try {
    } catch (e) {
      console.log('Invalid JSON');
      data = {};
    }

    switch (data.type) {
      case 'login':
        if (users[data.name]) {
          sendToOtherUser(conn, {
            type: 'login',
            success: false,
          });
        } else {
          users[data.name] = conn;
          conn.name = data.name;
          sendToOtherUser(conn, {
            type: 'login',
            success: true,
          });
        }

        break;
      case 'offer':
        var connect = users[data.name];
        if (connect != null) {
          conn.otherUser = data.name;

          sendToOtherUser(connect, {
            type: 'offer',
            offer: data.offer,
            name: data.name,
            from: data.from,
          });
        }
        break;

      case 'answer':
        var connect = users[data.name];

        if (connect != null) {
          conn.otherUser = data.name;
          sendToOtherUser(connect, {
            type: 'answer',
            answer: data.answer,
          });
        }

        break;

      case 'candidate':
        var connect = users[data.name];

        if (connect != null) {
          sendToOtherUser(connect, {
            type: 'candidate',
            candidate: data.candidate,
          });
        }
        break;
      case 'reject':
        var connect = users[data.name];

        if (connect != null) {
          sendToOtherUser(connect, {
            type: 'reject',
            name: conn.name,
          });
        }
        break;
      case 'accept':
        var connect = users[data.name];

        if (connect != null) {
          sendToOtherUser(connect, {
            type: 'accept',
            name: data.name,
          });
        }
        break;
      case 'leave':
        var connect = users[data.name];
        console.log('1', data);
        connect.otherUser = null;
        if (connect != null) {
          sendToOtherUser(connect, {
            type: 'leave',
          });
        }

        break;

      default:
        sendToOtherUser(conn, {
          type: 'error',
          message: 'Command not found: ' + data.type,
        });
        break;
    }
  });
  conn.on('close', function () {
    console.log('Connection closed');
    if (conn.name) {
      delete users[conn.name];
      if (conn.otherUser) {
        var connect = users[conn.otherUser];
        conn.otherUser = null;

        if (conn != null) {
          sendToOtherUser(connect, {
            type: 'leave',
          });
        }
      }
    }
  });

  conn.send('Hello World');
});

function sendToOtherUser(connection, message) {
  connection.send(JSON.stringify(message));
}

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(
  cors({
    credentials: true,
  })
);

//setup access-control-allow-origin
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Reequested-With, Content-type, Accept, Authorization'
  );
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    return res.status(200).json({});
  }
  next();
});

//For 500 - Error
app.use((error, req, res, next) => {
  console.log(error);
  res.status(500).json({
    error: error,
  });
});
