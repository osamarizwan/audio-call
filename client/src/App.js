import React from 'react';
import UdemyCall from './UdemyCall';

const App = () => {
  return (
    <div className='App'>
      <UdemyCall />
    </div>
  );
};

export default App;
