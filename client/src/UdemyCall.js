import React, { useEffect, useRef, useState } from 'react';

const UdemyCall = () => {
  const connection = new WebSocket('wss://ws-ws.herokuapp.com');
  const [data, setData] = useState();
  const [callerName, setCallerName] = useState();
  const [myConn, setMyConn] = useState();
  const setMYCONN = (val) => {
    myCONN.current = val;
    setMyConn(val);
  };
  const [dataChannel, setDataChannel] = useState();
  const [username, setUsername] = useState(sessionStorage.getItem('name'));
  const [stream, setStream] = useState();
  const [connected_user, setConnected_user] = useState();
  const [remote_video, setRemote_video] = useState(
    document.getElementById('remote-video')
  );
  const [callBtnSend, setCallBtnSend] = useState();
  const [call_to_username, setCall_to_username] = useState();
  const [call_btn, setCall_btn] = useState(document.getElementById('call-btn'));
  const [call_status, setCall_status] = useState();
  const [callRec, setCallRec] = useState();

  const [remoteConnection, setRemoteConnection] = useState();
  const remoteCONN = useRef(remoteConnection);
  const setREMOTECONN = (val) => {
    remoteCONN.current = val;
    setRemoteConnection(val);
  };
  connection.onopen = function () {
    console.log('Connected to the server');
  };

  const loginProcess = async (success) => {
    try {
      if (success === false) {
        alert('Try a different username');
      } else {
        const myStream = await navigator.mediaDevices.getUserMedia({
          video: true,
          audio: true,
        });
        setStream(myStream);
        const showVideo = document.getElementById('local-video');
        showVideo.srcObject = myStream;

        var configuration = {
          iceServers: [
            {
              url: 'stun:stun2.1.google.com:19302',
            },
          ],
        };
        const newConn = new RTCPeerConnection(configuration, {
          optional: [
            {
              RtpDataChannels: true,
            },
          ],
        });
        // setREMOTECONN(
        //   new RTCPeerConnection({
        //     offerToReceiveAudio: true,
        //     offerToReceiveVideo: true,
        //   })
        // );

        const newDataChannel = newConn.createDataChannel('channel1', {
          reliable: true,
        });
        setDataChannel(newDataChannel);
        newDataChannel.onerror = function (error) {
          console.log('Error: ', error);
        };
        newDataChannel.onmessage = function (event) {};
        newDataChannel.onclose = function () {
          console.log('data channel is closed');
        };

        console.log(stream);
        // myConn.addStream(stream);
        // newConn.ontrack = function (event) {
        newConn.onaddstream = function (event) {
          // newConn.addStream = (event) => {
          console.log('1', remote_video);
          console.log(event);
          remote_video.srcObject = event.stream;
        };

        newConn.onicecandidate = function (event) {
          if (event.candidate) {
            send({
              type: 'candidate',
              candidate: event.candidate,
            });
          }
        };
        setMYCONN(newConn);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const answerProcess = async (answer) => {
    console.log({ myCONN });
    console.log(answer);
    const createStream = await myCONN.current.setRemoteDescription(answer);
    console.log(createStream);
    myCONN.current.onaddstream = function (e) {
      console.log(e);
      // remote_video.srcObject = e.stream;
    };
    // .then(function () {
    //   console.log(createMyStream());
    // });
    // remoteCONN.current.setRemoteDescription(new RTCSessionDescription(answer));
    // console.log(remoteCONN);

    // console.log(myCONN.current.ontrack, myCONN.current.addStream);
    myCONN.current.ontrack = function (event) {
      //   // newConn.onaddstream = function (event) {
      //   // newConn.addStream = (event) => {
      console.log('1', event);
      //   console.log(event);
      //   remote_video.srcObject = event.stream;
    };
  };

  const myCONN = useRef(myConn);
  console.log(myCONN);
  useEffect(() => {
    connection.onmessage = (msg) => {
      var data = JSON.parse(msg.data);
      console.log(data);
      switch (data.type) {
        case 'login':
          loginProcess(data.success);
          break;
        case 'offer':
          console.log('offer');
          setCallerName(data.from);
          setData(data);
          break;
        case 'answer':
          console.log(myCONN);
          answerProcess(data.answer);
          break;
        case 'candidate':
          candidateProcess(data.candidate);
          break;
        case 'reject':
          rejectProcess();
          break;
        case 'accept':
          acceptProcess();
          break;
        case 'leave':
          leaveProcess();
          break;
        default:
          break;
      }
    };
  }, [myConn]);
  connection.onerror = function (error) {
    console.log(error);
  };

  const accept_call = () => {
    setCallerName();
    acceptCall(data.from);
    offerProcess(data.offer, data.from);
    setCallRec(true);
  };

  const videoCam = () => {
    stream.getVideoTracks()[0].enabled = !stream.getVideoTracks()[0].enabled;
    var video_toggle_class = document.querySelector('.video-toggle');
    if (video_toggle_class.innerText === 'videocam') {
      video_toggle_class.innerText = 'videocam_off';
    } else {
      video_toggle_class.innerText = 'videocam';
    }
  };

  const audioCam = () => {
    stream.getAudioTracks()[0].enabled = !stream.getAudioTracks()[0].enabled;
    var audio_toggle_class = document.getElementsByClassName('audio-toggle')[0];
    var video_toggle_class = document.getElementsByClassName('video-toggle')[0];
    console.log(audio_toggle_class, video_toggle_class, stream);
    if (video_toggle_class.innerText === 'mic') {
      video_toggle_class.innerText = 'mic_off';
    } else {
      video_toggle_class.innerText = 'mic';
    }
  };

  useEffect(() => {
    loginProcess();
    setTimeout(function () {
      if (connection.readyState === 1) {
        console.log('ready');
        if (username != null) {
          console.log('send');
          send({
            type: 'login',
            name: username,
          });
        }
      } else {
        console.log('Connection has not stublished');
      }
    }, 3000);
  }, []);

  const callRejectHandler = () => {
    setCall_status('');
    call_status.innerHTML = '';
    alert('Call is rejected');
    rejectedCall(document.getElementById('username-input').value);
  };

  function callBtn() {
    var call_to_username = document.getElementById('username-input').value;
    console.log(call_to_username);

    setCall_to_username(call_to_username);
    setCallBtnSend(true);
    console.log(document.getElementsByClassName('call-reject'));

    if (call_to_username.length > 0) {
      setConnected_user(call_to_username);
      myConn.createOffer(
        function (offer) {
          send({
            type: 'offer',
            offer: offer,
            name: call_to_username,
            from: username,
          });
          myConn.setLocalDescription(offer);
        },
        function (error) {
          alert('Offer has not created');
        }
      );
    }
  }

  function send(message) {
    if (connected_user) {
      message.name = connected_user;
    }
    connection.send(JSON.stringify(message));
  }

  const offerProcess = (offer, name) => {
    setConnected_user(name);
    myConn.setRemoteDescription(new RTCSessionDescription(offer));
    myConn.createAnswer(
      (answer) => {
        console.log('object', answer);
        myConn.setLocalDescription(answer);
        send({
          type: 'answer',
          answer: answer,
          name,
        });
      },
      (error) => {
        alert('Answer has not created');
      }
    );
  };

  const candidateProcess = (candidate) => {
    myConn.addIceCandidate(new RTCIceCandidate(candidate));
  };

  const rejectedCall = (rejected_caller_or_callee) => {
    send({
      type: 'reject',
      name: rejected_caller_or_callee,
    });
  };

  const acceptCall = (callee_name) => {
    send({
      type: 'accept',
      name: callee_name,
    });
  };

  const rejectProcess = () => {
    setCallerName();
  };

  const acceptProcess = () => {
    setCallerName();
  };

  const hangup = () => {
    const callCancel = document.getElementsByClassName('call-cancel');
  };

  function callCancel() {
    setCallerName();
    send({
      type: 'leave',
    });
    leaveProcess();
  }
  const leaveProcess = () => {
    document.getElementById('remote-video').src = null;
    myConn.close();
    myConn.onicecandidate = null;
    myConn.onaddstream = null;
    setConnected_user(null);
  };
  return (
    <div className='main-wrap'>
      <div className='call-wrap card' style={{ zIndex: '99px' }}>
        <video id='local-video' autoPlay></video>
        <div className='row remote-video-wrap' style={{ position: 'relative' }}>
          <div
            className='call-hang-status'
            style={{ position: 'absolute', zIndex: '999px' }}
          >
            {callerName ? (
              <div className='calling-status-wrap card black white-text'>
                <div className='user-image'>
                  <img
                    src='assets/images/me.jpg'
                    className='caller-image circle'
                    alt=''
                  />
                </div>
                <div className='user-name'> {callerName}</div>
                <div className='user-calling-status'>Calling...</div>
                <div className='calling-action'>
                  <div onClick={accept_call} className='call-accept'>
                    <i className='material-icons green darken-2 white-text audio-icon'>
                      Receive
                    </i>
                  </div>
                  <div className='call-reject'>
                    <i className='material-icons red darken-3 white-text close-icon'>
                      Close
                    </i>
                  </div>
                </div>
              </div>
            ) : callRec ? (
              <div className='call-status-wrap white-text'>
                <div className='calling-wrap'>
                  <div className='calling-hang-action'>
                    <div onClick={videoCam} className='videocam-on'>
                      <i className='material-icons teal darken-2 white-text video-toggle'>
                        videocam
                      </i>
                    </div>
                    <div onClick={audioCam} className='audio-on'>
                      <i className='material-icons teal darken-2 white-text audio-toggle'>
                        mic
                      </i>
                    </div>
                    <div onClick={callCancel} className='call-cancel'>
                      <i className='call-cancel-icon material-icons red darken-3 white-text'>
                        call
                      </i>
                    </div>
                  </div>
                </div>
              </div>
            ) : null}
          </div>
          <video id='remote-video' autoPlay></video>
        </div>
        <div className='row'>
          <div className='col m12'>
            <input
              type='text'
              name=''
              id='username-input'
              placeholder='Username'
            />
            <button
              onClick={callBtn}
              id='call-btn'
              className='btn waves-effect'
            >
              {callBtnSend ? (
                <div className='calling-status-wrap card black white-text'>
                  <div className='user-image'>
                    <img
                      src='assets/images/other.jpg'
                      className='caller-image circle'
                      alt=''
                    />
                  </div>
                  <div className='user-name'>{call_to_username}</div>
                  <div className='user-calling-status'>Calling...</div>
                  <div className='calling-action'>
                    <div onClick={callRejectHandler} className='call-reject'>
                      <i className='material-icons red darken-3 white-text close-icon'>
                        close
                      </i>
                    </div>
                  </div>
                </div>
              ) : (
                'Call'
              )}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default UdemyCall;
