import React from 'react';

const AddMsg = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <input type='text' required onChange={(e) => props.handleChange(e)} />
      <input type='button' name='Send' value='Send' />
    </form>
  );
};

export default AddMsg;
