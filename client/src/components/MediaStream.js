import { useState, useEffect } from 'react';

export const useUserMedia = () => {
  const [mediaStream, setMediaStream] = useState(null);

  useEffect(() => {
    const enableStream = async () => {
      try {
        const stream = await navigator.mediaDevices.getUserMedia({
          audio: true,
          video: true,
        });
        console.log(stream);
        setMediaStream(stream);
      } catch (err) {
        console.log(err);
        // Removed for brevity
      }
    };

    if (!mediaStream) {
      enableStream();
      console.log(enableStream());
    } else {
      return function cleanup() {
        mediaStream.getTracks().forEach((track) => {
          track.stop();
        });
      };
    }
  }, [mediaStream]);

  return mediaStream;
};
