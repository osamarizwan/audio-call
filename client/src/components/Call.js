import { Send } from '@material-ui/icons';
import { message } from 'antd';
import React, { useState, useEffect } from 'react';
// import WebSocket from 'ws';
import { useUserMedia } from './MediaStream';

const Call = () => {
  const ws = new WebSocket('ws://192.168.88.29:8000');
  const [connectedUser, setConnectedUser] = useState();
  const [name, setName] = useState(localStorage.getItem('name') || 'mor');
  const [mediaStream, setMediaStream] = useState(null);
  // const mediaStream = useUserMedia();
  console.log(name);
  const loginProcess = async (success) => {
    try {
      if (success === false) {
        alert('Try a different username');
      } else {
        console.log('hi');
        const stream = await navigator.mediaDevices.getUserMedia({
          audio: true,
          video: true,
        });
        const showVideo = document.getElementById('video');
        // console.log(stream);
        console.log(showVideo);
        // showVideo.srcObject = stream;
        // setMediaStream(stream);
      }
    } catch (err) {
      console.log(err);
      // Removed for brevity
    }
  };

  console.log(ws);
  ws.onopen = () => {
    console.log('connected');
    // ws.send(Date.now());
  };

  ws.onmessage = (msg) => {
    console.log('Msg : ', msg);
    let data = msg.data;
    switch (data.type) {
      case 'login':
        loginProcess(data.success);
        break;
      default:
        loginProcess('false');
    }
  };

  ws.onerror = (error) => {
    console.log('Error : ', error);
    // ws.send(Date.now());
  };

  const send = (msg) => {
    msg.name = 'mor';
    ws.send(msg);
  };

  useEffect(() => {
    if (ws.readyState === 1) {
      send({
        type: 'user',
        name,
      });
    }
  }, []);
  return (
    <div>
      <div>
        <audio id='local-audio' autoPlay></audio>
        <div>
          <div id='callStatus'></div>
          <video
            id='video'
            style={{ display: 'none', transform: 'rotateY(180deg)' }}
            autoPlay
          ></video>
        </div>
        <div>
          <div>
            <form>
              <input type='text' name='' id='username' placeholder='username' />
              <input type='submit' name='Call' value='Call' />
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Call;
