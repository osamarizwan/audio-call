import React from 'react';

const GetMsg = (props) => {
  console.log(props.allMsgs);
  return (
    <div>
      <h2>Chat's message</h2>
      {props.allMsgs ? (
        props.allMsgs?.map((data) => {
          return <h6 key={data._id}>{data.msg}</h6>;
        })
      ) : (
        <h5>There is no preevious chat</h5>
      )}
    </div>
  );
};
export default GetMsg;
