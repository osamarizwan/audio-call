import React, { useEffect, useRef, useState } from 'react';

const UdemyCall = () => {
  const connection = new WebSocket('ws://192.168.88.29:8000');
  const [name, setName] = useState();
  const [data, setData] = useState();
  const [callerName, setCallerName] = useState();
  const [connectedUser, setConnectedUser] = useState();
  const [myConn, setMyConn] = useState();
  const setMYCONN = (val) => {
    myCONN.current = val;
    setMyConn(val);
  };
  const [dataChannel, setDataChannel] = useState();
  const [username, setUsername] = useState(sessionStorage.getItem('name'));
  const [stream, setStream] = useState();
  const [connected_user, setConnected_user] = useState();
  const [local_video, setLocal_video] = useState(
    document.getElementById('local-video')
  );
  const [remote_video, setRemote_video] = useState(
    document.getElementById('remote-video')
  );
  const [call_btn, setCall_btn] = useState(document.getElementById('call-btn'));
  const [call_to_username_input, setCall_to_username_input] = useState(
    document.getElementById('username-input')
  );
  const [call_status, setCall_status] = useState();
  // document.getElementsByClassName('call-hang-status')
  const [video_toggle_class, setVideo_toggle_class] =
    document.getElementsByClassName('video-toggle');
  const [chatArea, setChatArea] = useState();
  const [callRec, setCallRec] = useState();
  console.log(username);

  connection.onopen = function () {
    console.log('Connected to the server');
  };

  const loginProcess = async (success) => {
    try {
      console.log('object');
      if (success === false) {
        alert('Try a different username');
      } else {
        console.log('object');
        const myStream = await navigator.mediaDevices.getUserMedia({
          video: true,
          audio: true,
        });

        setStream(myStream);
        const showVideo = document.getElementById('local-video');
        showVideo.srcObject = myStream;

        var configuration = {
          iceServers: [
            {
              url: 'stun:stun2.1.google.com:19302',
            },
          ],
        };
        const newConn = new RTCPeerConnection(configuration, {
          optional: [
            {
              RtpDataChannels: true,
            },
          ],
        });
        const remoteConnection = new RTCPeerConnection({
          offerToReceiveAudio: true,
          offerToReceiveVideo: true,
        });
        console.log(newConn);
        setMYCONN(newConn);

        const newDataChannel = newConn.createDataChannel('channel1', {
          reliable: true,
        });
        setDataChannel(newDataChannel);
        newDataChannel.onerror = function (error) {
          console.log('Error: ', error);
        };
        console.log(newDataChannel);
        newDataChannel.onmessage = function (event) {
          console.log(event);
          document.getElementById('chat-area').innerHTML +=
            "<div class='left-align' style='display:flex;align-items:center;'><img src='assets/images/other.jpg' style='height:40px;width:40px;' class='caller-image circle'><div style='font-weight:600;margin:0 5px;'>" +
            connected_user +
            '</div>: <div>' +
            event.data +
            '</div></div><br/>';
        };
        newDataChannel.onclose = function () {
          console.log('data channel is closed');
        };
        console.log(remoteConnection, newConn.ontrack, newConn.addStream);
        newConn.ontrack = function (event) {
          // newConn.onaddstream = function (event) {
          // newConn.addStream = (event) => {
          console.log('1', remote_video);
          console.log(event);
          remote_video.srcObject = event.stream;

          // call_status.innerHTML =
          //   '<div class="call-status-wrap white-text"> <div class="calling-wrap"> <div class="calling-hang-action"> <div class="videocam-on"> <i class="material-icons teal darken-2 white-text video-toggle">videocam</i> </div> <div class="audio-on"> <i class="material-icons teal darken-2 white-text audio-toggle">mic</i> </div> <div class="call-cancel"> <i class="call-cancel-icon material-icons red darken-3 white-text">call</i> </div> </div> </div> </div>';

          // var video_toggle = document.querySelector('.videocam-on');
          // var audio_toggle = document.querySelector('.audio-on');
          // video_toggle.onclick = function () {
          //   myStream.getVideoTracks()[0].enabled =
          //     !myStream.getVideoTracks()[0].enabled;

          //   var video_toggle_class = document.querySelector('.video-toggle');
          //   if (video_toggle_class.innerText === 'videocam') {
          //     video_toggle_class.innerText = 'videocam_off';
          //   } else {
          //     video_toggle_class.innerText = 'videocam';
          //   }
          // };

          // audio_toggle.onclick = function () {
          //   stream.getAudioTracks()[0].enabled =
          //     !stream.getAudioTracks()[0].enabled;

          //   var audio_toggle_class = document.querySelector('.audio-toggle');
          //   if (video_toggle_class.innerText === 'mic') {
          //     video_toggle_class.innerText = 'mic_off';
          //   } else {
          //     video_toggle_class.innerText = 'mic';
          //   }
          // };
          // hangup();
        };

        newConn.onicecandidate = function (event) {
          console.log('object');
          if (event.candidate) {
            send({
              type: 'candidate',
              candidate: event.candidate,
            });
          }
        };
      }
    } catch (error) {
      console.log(error);
    }
  };

  const answerProcess = (answer) => {
    console.log({ answer });
    console.log({ myCONN });
    myCONN.current.setRemoteDescription(new RTCSessionDescription(answer));
    // console.log(myCONN.current.ontrack, myCONN.current.addStream);
    // myCONN.current.ontrack = function (event) {
    //   // newConn.onaddstream = function (event) {
    //   // newConn.addStream = (event) => {
    //   console.log('1', remote_video);
    //   console.log(event);
    //   remote_video.srcObject = event.stream;
    // };
  };

  const myCONN = useRef(myConn);

  useEffect(() => {
    connection.onmessage = (msg) => {
      var data = JSON.parse(msg.data);
      console.log(data);
      console.log({ myConn, myCONN });
      switch (data.type) {
        case 'login':
          loginProcess(data.success);
          break;
        case 'offer':
          console.log('offer');
          console.log(call_status);
          setCallerName(data.from);
          console.log(data.name);
          setData(data);
          // setCall_status(
          //   '<div className="calling-status-wrap card black white-text"> <div className="user-image"> <img src="assets/images/me.jpg" className="caller-image circle" alt=""> </div> <div className="user-name">' +
          //     data.name +
          //     '</div> <div className="user-calling-status">Calling...</div> <div className="calling-action"> <div className="call-accept"><i className="material-icons green darken-2 white-text audio-icon">call</i></div> <div className="call-reject"><i className="material-icons red darken-3 white-text close-icon">close</i></div> </div> </div>'
          // );
          // call_status.innerHTML =
          //   '<div className="calling-status-wrap card black white-text"> <div className="user-image"> <img src="assets/images/me.jpg" className="caller-image circle" alt=""> </div> <div className="user-name">' +
          //   data.name +
          //   '</div> <div className="user-calling-status">Calling...</div> <div className="calling-action"> <div className="call-accept"><i className="material-icons green darken-2 white-text audio-icon">call</i></div> <div className="call-reject"><i className="material-icons red darken-3 white-text close-icon">close</i></div> </div> </div>';
          // var call_receive = document.getElementsByClassName('call-accept');
          // console.log(call_receive);
          // var call_reject = document.getElementsByClassName('call-reject');
          // console.log(call_reject);
          // document
          //   .getElementsByClassName('call-accept')[0]
          //   .addEventListener('click', function () {
          //     acceptCall(data.name);
          //     console.log({ myConn });
          //     offerProcess(data.offer, data.name);
          //     call_status.innerHTML =
          //       '<div class="call-status-wrap white-text"> <div class="calling-wrap"> <div class="calling-hang-action"> <div class="videocam-on"> <i class="material-icons teal darken-2 white-text video-toggle">videocam</i> </div> <div class="audio-on"> <i class="material-icons teal darken-2 white-text audio-toggle">mic</i> </div> <div class="call-cancel"> <i class="call-cancel-icon material-icons red darken-3 white-text">call</i> </div> </div> </div> </div>';

          //     var video_toggle = document.querySelector('.videocam-on');
          //     var audio_toggle = document.querySelector('.audio-on');
          //     video_toggle.onclick = function () {
          //       stream.getVideoTracks()[0].enabled =
          //         !stream.getVideoTracks()[0].enabled;

          //       var video_toggle_class =
          //         document.querySelector('.video-toggle');
          //       if (video_toggle_class.innerText === 'videocam') {
          //         video_toggle_class.innerText = 'videocam_off';
          //       } else {
          //         video_toggle_class.innerText = 'videocam';
          //       }
          //     };

          //     audio_toggle.onclick = function () {
          //       stream.getAudioTracks()[0].enabled =
          //         !stream.getAudioTracks()[0].enabled;

          //       var audio_toggle_class =
          //         document.querySelector('.audio-toggle');
          //       if (video_toggle_class.innerText === 'mic') {
          //         video_toggle_class.innerText = 'mic_off';
          //       } else {
          //         video_toggle_class.innerText = 'mic';
          //       }
          //     };
          //     // hangup();
          //   });
          // document
          //   .getElementsByClassName('call-reject')[0]
          //   .addEventListener('click', function () {
          //     console.log(call_status);
          //     call_status.innerHTML = '';
          //     alert('Call is rejected');
          //     rejectedCall(data.name);
          //   });

          break;
        case 'answer':
          console.log(myCONN);
          // myConn.setRemoteDescription(new RTCSessionDescription(answer));
          answerProcess(data.answer);
          break;
        case 'candidate':
          candidateProcess(data.candidate);
          break;
        case 'reject':
          rejectProcess();
          break;
        case 'accept':
          acceptProcess();
          break;
        case 'leave':
          leaveProcess();
          break;
        default:
          break;
      }
    };
  }, [myConn]);
  connection.onerror = function (error) {
    console.log(error);
  };
  console.log(call_btn);

  const accept_call = () => {
    setCallerName();
    acceptCall(data.from);
    console.log({ myConn });
    offerProcess(data.offer, data.from);

    // call_status.innerHTML =
    //   '<div class="call-status-wrap white-text"> <div class="calling-wrap"> <div class="calling-hang-action"> <div class="videocam-on"> <i class="material-icons teal darken-2 white-text video-toggle">videocam</i> </div> <div class="audio-on"> <i class="material-icons teal darken-2 white-text audio-toggle">mic</i> </div> <div class="call-cancel"> <i class="call-cancel-icon material-icons red darken-3 white-text">call</i> </div> </div> </div> </div>';
    setCallRec(true);
    // var video_toggle = document.querySelector('.videocam-on');
    // const video_toggle1 = document.getElementsByClassName('videocam-on')[0];
    // console.log(video_toggle, video_toggle1);
    // var audio_toggle = document.querySelector('.audio-on');
    // video_toggle.onclick = function () {
    //   stream.getVideoTracks()[0].enabled = !stream.getVideoTracks()[0].enabled;

    //   var video_toggle_class = document.querySelector('.video-toggle');
    //   if (video_toggle_class.innerText === 'videocam') {
    //     video_toggle_class.innerText = 'videocam_off';
    //   } else {
    //     video_toggle_class.innerText = 'videocam';
    //   }
    // };

    // audio_toggle.onclick = function () {
    //   stream.getAudioTracks()[0].enabled = !stream.getAudioTracks()[0].enabled;

    //   var audio_toggle_class = document.querySelector('.audio-toggle');
    //   if (video_toggle_class.innerText === 'mic') {
    //     video_toggle_class.innerText = 'mic_off';
    //   } else {
    //     video_toggle_class.innerText = 'mic';
    //   }
    // };
    // hangup();
  };

  const videoCam = () => {
    stream.getVideoTracks()[0].enabled = !stream.getVideoTracks()[0].enabled;

    var video_toggle_class = document.querySelector('.video-toggle');
    if (video_toggle_class.innerText === 'videocam') {
      video_toggle_class.innerText = 'videocam_off';
    } else {
      video_toggle_class.innerText = 'videocam';
    }
  };

  const audioCam = () => {
    stream.getAudioTracks()[0].enabled = !stream.getAudioTracks()[0].enabled;

    var audio_toggle_class = document.getElementsByClassName('audio-toggle')[0];
    var video_toggle_class = document.getElementsByClassName('video-toggle')[0];
    console.log(audio_toggle_class, video_toggle_class, stream);
    if (video_toggle_class.innerText === 'mic') {
      video_toggle_class.innerText = 'mic_off';
    } else {
      video_toggle_class.innerText = 'mic';
    }
  };

  useEffect(() => {
    loginProcess();
    setTimeout(function () {
      if (connection.readyState === 1) {
        console.log('ready');
        if (username != null) {
          console.log('send');
          send({
            type: 'login',
            name: username,
          });
        }
      } else {
        console.log('Connection has not stublished');
      }
    }, 3000);
  }, []);

  function callBtn() {
    console.log('call');
    var call_to_username = document.getElementById('username-input').value;
    document.getElementById('call-btn').innerHTML =
      '<div class="calling-status-wrap card black white-text"> <div class="user-image"> <img src="assets/images/other.jpg" class="caller-image circle" alt=""> </div> <div class="user-name">' +
      call_to_username +
      '</div> <div class="user-calling-status">Calling...</div> <div class="calling-action"> <div class="call-reject"><i class="material-icons red darken-3 white-text close-icon">close</i></div> </div> </div>';
    var call_reject = document.getElementsByClassName('call-reject');
    console.log(call_reject);
    document
      .querySelector('.call-reject')
      .addEventListener('click', function () {
        console.log(call_status);
        setCall_status('');
        call_status.innerHTML = '';
        alert('Call is rejected');
        rejectedCall(document.getElementById('username-input').value);
      });

    if (call_to_username.length > 0) {
      setConnected_user(call_to_username);
      console.log(myConn);
      myConn.createOffer(
        function (offer) {
          send({
            type: 'offer',
            offer: offer,
            name: call_to_username,
            from: username,
          });
          myConn.setLocalDescription(offer);
          console.log(offer);
          console.log(myConn);
        },
        function (error) {
          alert('Offer has not created');
        }
      );
    }
  }

  function send(message) {
    console.log(message);
    console.log(connected_user);
    if (connected_user) {
      console.log(connected_user);
      message.name = connected_user;
    }
    connection.send(JSON.stringify(message));
  }

  const offerProcess = (offer, name) => {
    console.log(myConn);
    console.log(offer, name);
    setConnected_user(name);
    console.log(myConn);
    console.log({ myCONN });
    myConn.setRemoteDescription(new RTCSessionDescription(offer));
    myConn.createAnswer(
      (answer) => {
        console.log('object', answer);
        myConn.setLocalDescription(answer);
        send({
          type: 'answer',
          answer: answer,
          name,
        });
      },
      (error) => {
        alert('Answer has not created');
      }
    );
    console.log(myConn);
  };

  const candidateProcess = (candidate) => {
    myConn.addIceCandidate(new RTCIceCandidate(candidate));
  };

  const rejectedCall = (rejected_caller_or_callee) => {
    send({
      type: 'reject',
      name: rejected_caller_or_callee,
    });
  };

  const acceptCall = (callee_name) => {
    send({
      type: 'accept',
      name: callee_name,
    });
  };

  const rejectProcess = () => {
    setCallerName();
  };

  const acceptProcess = () => {
    setCallerName();
  };

  const hangup = () => {
    // var call_cancel = document.querySelector('.call-cancel');
    const callCancel = document.getElementsByClassName('call-cancel');
    console.log(callCancel);
  };

  function callCancel() {
    // call_status.innerHTML = '';
    setCallerName();
    send({
      type: 'leave',
    });
    leaveProcess();
  }
  const leaveProcess = () => {
    console.log(myConn);
    document.getElementById('remote-video').src = null;
    myConn.close();
    myConn.onicecandidate = null;
    myConn.onaddstream = null;
    console.log(connected_user);
    setConnected_user(null);
  };
  console.log(myConn);
  return (
    <div className='main-wrap'>
      <div className='call-wrap card' style={{ zIndex: '99px' }}>
        <video id='local-video' autoPlay></video>
        <div className='row remote-video-wrap' style={{ position: 'relative' }}>
          <div
            className='call-hang-status'
            style={{ position: 'absolute', zIndex: '999px' }}
          >
            {callerName ? (
              <div className='calling-status-wrap card black white-text'>
                <div className='user-image'>
                  <img
                    src='assets/images/me.jpg'
                    className='caller-image circle'
                    alt=''
                  />
                </div>
                <div className='user-name'> {callerName}</div>
                <div className='user-calling-status'>Calling...</div>
                <div className='calling-action'>
                  <div onClick={accept_call} className='call-accept'>
                    <i className='material-icons green darken-2 white-text audio-icon'>
                      call
                    </i>
                  </div>
                  <div className='call-reject'>
                    <i className='material-icons red darken-3 white-text close-icon'>
                      close
                    </i>
                  </div>
                </div>
              </div>
            ) : callRec ? (
              <div className='call-status-wrap white-text'>
                <div className='calling-wrap'>
                  <div className='calling-hang-action'>
                    <div onClick={videoCam} className='videocam-on'>
                      <i className='material-icons teal darken-2 white-text video-toggle'>
                        videocam
                      </i>
                    </div>
                    <div onClick={audioCam} className='audio-on'>
                      <i className='material-icons teal darken-2 white-text audio-toggle'>
                        mic
                      </i>
                    </div>
                    <div onClick={callCancel} className='call-cancel'>
                      <i className='call-cancel-icon material-icons red darken-3 white-text'>
                        call
                      </i>
                    </div>
                  </div>
                </div>
              </div>
            ) : null}
          </div>
          <video id='remote-video' autoPlay></video>
        </div>
        <div className='row'>
          <div className='col m12'>
            <input
              type='text'
              name=''
              id='username-input'
              placeholder='Username'
            />
            <button
              onClick={callBtn}
              id='call-btn'
              className='btn waves-effect'
            >
              Call
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default UdemyCall;
