import React, { useEffect, useState } from 'react';
import { io } from 'socket.io-client';

import All from './All';
import AddMsg from './AddMsg';
import axios from 'axios';
import GetMsg from './GetMsg';

const Home = () => {
  const socket = io('http://192.168.88.29:7000');
  // const socket = io.connect('http://localhost:7000');
  const [msg, setMsg] = useState();
  const [allMsgs, setAllMsgs] = useState([]);

  console.log('connecting user to socket server');
  useEffect(() => {
    axios
      .get('http://192.168.88.29:7000/api/all')
      .then((data) => {
        console.log(data);
        setAllMsgs(data.data.result);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    socket.on('connect', () => {
      console.log('connected');
    });
  }, []);

  useEffect(() => {
    socket.on('msg', (data) => {
      setAllMsgs([...allMsgs, data.savedData]);
      console.log(data);
    });
  }, [msg]);

  // socket.on('error', function () {
  //   console.log('there was an error');
  // });

  // socket.on('connect', function () {
  //   console.log('socket connection established');
  //   // console.log("connecting to " + _room)
  //   // socket.emit('add', );
  // });
  // socket.on('addMsg', (socket) => {
  //   console.log('connected', socket);
  // });

  const handleChange = (e) => {
    setMsg(e.target.value);
    console.log('1');
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(msg);
    socket.emit('addMsg', msg);
  };
  return (
    <>
      <h1>Real Time</h1>
      <GetMsg allMsgs={allMsgs} />
      <AddMsg
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        msg={msg}
        setMsg={setMsg}
      />
    </>
  );
};

export default Home;
